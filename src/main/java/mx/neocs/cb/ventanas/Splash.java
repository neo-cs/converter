/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.cb.ventanas;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.SplashScreen;
import java.util.logging.Logger;

/**
 * @since 3.6.2
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Splash {

    private static final Logger LOG = Logger.getLogger(Splash.class.getName());

    private static void renderSplashFrame(Graphics2D graphics2D, int frame) {
        final String[] comps = {"interfaz", "menus", "preparando recursos",
            "componentes del menu", "funciones"};
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setComposite(AlphaComposite.Clear);
        graphics2D.fillRect(220, 310, 260, 40);
        graphics2D.setPaintMode();
        graphics2D.setColor(Color.WHITE);
        graphics2D.drawString("Cargando " + comps[(frame / 8) % 5] + "...", 220, 350);
        graphics2D.fillRect(220, 310, (frame * 10) % 260, 20);
    }

    /**
     * Este método permite que se muestre el Splash al iniciar la aplicación.
     */
    public static void loadSplash() {
        final SplashScreen splash = SplashScreen.getSplashScreen();

        if (splash == null) {
            LOG.fine("SplashScreen.getSplashScreen() returned null");
            return;
        }

        Graphics2D graphics2D = splash.createGraphics();

        if (graphics2D == null) {
            LOG.fine("g is null");
            return;
        }

        for (int i = 0; i < 100; i++) {
            renderSplashFrame(graphics2D, i);
            splash.update();
            try {
                /*
                 * Para evitar el Thread.sleep puedo apoyarme de:
                 * https://www.youtube.com/watch?v=x9DeOLtVCuQ
                 */
                Thread.sleep(100);
            } catch (InterruptedException e) {
                LOG.severe(e.getMessage());
            }
        }

        splash.close();
    }

}
