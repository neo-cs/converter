/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.cb.ventanas;

import java.awt.HeadlessException;
import mx.neocs.cb.util.Convertidor;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;

/**
 * Crea la interfaz GUI que el usuario usara para realizar las converciones
 *
 * @author Freddy Barrera (freddy.barrera.moo@hotmail.com)
 * @author Roberto Lili Che
 */
public class Ventana extends javax.swing.JFrame {

    private static final long serialVersionUID = 6478487674224482038L;

    public Ventana() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTxtNumero = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTxtBase = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jBtnCalcular = new javax.swing.JButton();
        jBtnLimpiar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLblResultado = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTxtAreaCociente = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTxtAreaResiduo = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTxtAreaProducto = new javax.swing.JTextArea();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTxtAreaEnteroProducto = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Convertidor de base...");
        setIconImage(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("mx/neocs/cb/imagenes/calculadora.png")).getImage());
        setLocationByPlatform(true);
        setResizable(false);

        jLabel1.setText("Catidad decimal:");

        jTxtNumero.setToolTipText("Ingrese una cantidad decimal (ejemplo: 45.78)");
        jTxtNumero.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtNumeroFocusGained(evt);
            }
        });
        jTxtNumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtNumeroKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtNumeroKeyReleased(evt);
            }
        });

        jLabel2.setText("Base:");

        jTxtBase.setToolTipText("Ingrese una base (ejemplo para convertir a binario ingrese 2)");

        jSeparator1.setPreferredSize(new java.awt.Dimension(0, 3));

        jBtnCalcular.setText("Calcular");
        jBtnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnCalcularActionPerformed(evt);
            }
        });

        jBtnLimpiar.setText("Limpiar");
        jBtnLimpiar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnLimpiarMouseClicked(evt);
            }
        });

        jLabel5.setText("El número es:");

        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.RIGHT);
        jTabbedPane1.setAutoscrolls(true);

        jLabel3.setText("Cociente:");

        jTxtAreaCociente.setColumns(20);
        jTxtAreaCociente.setEditable(false);
        jTxtAreaCociente.setRows(5);
        jScrollPane1.setViewportView(jTxtAreaCociente);

        jTxtAreaResiduo.setColumns(20);
        jTxtAreaResiduo.setEditable(false);
        jTxtAreaResiduo.setRows(5);
        jScrollPane2.setViewportView(jTxtAreaResiduo);

        jLabel4.setText("Reciduo:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Enteros", null, jPanel1, "Muestra el proceso de conversión de la parte entera.");

        jTxtAreaProducto.setColumns(20);
        jTxtAreaProducto.setEditable(false);
        jTxtAreaProducto.setRows(5);
        jScrollPane3.setViewportView(jTxtAreaProducto);

        jLabel6.setText("Productos:");

        jLabel7.setText("Parte entera del producto:");

        jTxtAreaEnteroProducto.setColumns(20);
        jTxtAreaEnteroProducto.setEditable(false);
        jTxtAreaEnteroProducto.setRows(5);
        jScrollPane4.setViewportView(jTxtAreaEnteroProducto);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addContainerGap(95, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addGap(11, 11, 11)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Fraccionarios", null, jPanel2, "Muestra el proceso de conversión de la parte fracionaria.");

        jTextArea1.setColumns(20);
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setText("1) Parte entera. Divida N(i) y cada cociente sucesivo por b hasta obtener un cociente cero. La ducesión de residuos, en orden inverso, da la representación en la base b de N(i).\n\n2) Parte fraccionaria. Multiplique N(f) y la parte fraccionaria de cada producto sucesivo por b, hasta obtener una parte fraccionaria cero o una parte fraccionaria duplicada. Entonces la sucesión infinita o finita periodica de las pasrtes enteras de la representación en base b de N(f).");
        jTextArea1.setWrapStyleWord(true);
        jScrollPane5.setViewportView(jTextArea1);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Algoritmo", null, jPanel3, "Muestra el algoritmo de conversión.");

        jMenu2.setText("Archivo");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem1.setText("Salir");
        jMenuItem1.setActionCommand("&Salir");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuBar1.add(jMenu2);

        jMenu1.setText("Ayuda");

        jMenuItem2.setText("A cerca de...");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 355, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jBtnLimpiar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBtnCalcular))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLblResultado, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jTxtNumero, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                                .addComponent(jTxtBase)))))
                .addContainerGap())
            .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTxtBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTxtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLblResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtnCalcular)
                    .addComponent(jBtnLimpiar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private boolean esValido(Character character) {
        return Character.isDigit(character) || character == KeyEvent.VK_BACK_SPACE || character == KeyEvent.VK_PERIOD;
    }

    private void limpiarTodo() {
        jTxtNumero.setText("");
        jTxtBase.setText("");
        jTxtAreaCociente.setText("");
        jTxtAreaResiduo.setText("");
        jTxtAreaProducto.setText("");
        jTxtAreaEnteroProducto.setText("");
        jLblResultado.setText("");
        jTxtBase.setEditable(true);
        jTxtNumero.setEditable(true);
    }

    private void calcular() {
        try {
            if (!jTxtNumero.getText().isEmpty() && (!jTxtBase.getText().isEmpty())) {
                jTxtBase.setEditable(false);
                jTxtNumero.setEditable(false);
                Convertidor funcion = new Convertidor();

                funcion.convertir(Integer.parseInt(jTxtBase.getText()),jTxtNumero.getText());

                jTxtAreaCociente.setText(funcion.getCocientes());
                jTxtAreaResiduo.setText(funcion.getResiduos());
                jTxtAreaEnteroProducto.setText(funcion.getEnteroProducto());
                jTxtAreaProducto.setText(funcion.getFraccionProducto());

                jLblResultado.setText(
                        funcion.getNuevoEntero() + funcion.getNuevoFraccionario());
            } else {
                JOptionPane.showMessageDialog(null, "Debe ingresar " +
                        "una base y un numero", "Advertencia",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (HeadlessException | NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Por favor vuelva a teclear " +
                    "sus valores", "Error",
                    JOptionPane.ERROR_MESSAGE);
            limpiarTodo();
        }
    }

    private void jBtnLimpiarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnLimpiarMouseClicked
        limpiarTodo();
    }//GEN-LAST:event_jBtnLimpiarMouseClicked

    private void jBtnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnCalcularActionPerformed
        calcular();
    }//GEN-LAST:event_jBtnCalcularActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new AcercaDe().setVisible(true);
            }
        });
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jTxtNumeroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNumeroKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            calcular();
        }
    }//GEN-LAST:event_jTxtNumeroKeyPressed

    private void jTxtNumeroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNumeroKeyReleased
//        if (!(evt.getKeyCode() == KeyEvent.VK_ENTER)) {
//            Character caracter = evt.getKeyChar();
//            int val = Integer.parseInt(jTxtBase.getText());
//            int a = ((int) evt.getKeyChar()) - 48;
//
//            if ((a >= val) || !esValido(caracter)) {
//                jLabel8.setText("Valor invalido");
//                String texto = "";
//                for (int i = 0; i < jTxtNumero.getText().length(); i++) {
//                    if (esValido(jTxtNumero.getText().charAt(i)) & ((jTxtNumero.getText().charAt(i) - 48) < val)) {
//                        texto += jTxtNumero.getText().charAt(i);
//                    }
//                }
//                jTxtNumero.setText(texto);
//                getToolkit().beep();
//
//                Timer tm = new Timer();
//
//                tm.schedule(new TimerTask() {
//
//                    @Override
//                    public void run() {
//                        jLabel8.setText("");
//                    }
//                }, 1000);
//            }
//        }
    }//GEN-LAST:event_jTxtNumeroKeyReleased

    private void jTxtNumeroFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtNumeroFocusGained
        if (!jTxtBase.getText().isEmpty()) {
            jTxtBase.setEditable(false);
        }
    }//GEN-LAST:event_jTxtNumeroFocusGained


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnCalcular;
    private javax.swing.JButton jBtnLimpiar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLblResultado;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTxtAreaCociente;
    private javax.swing.JTextArea jTxtAreaEnteroProducto;
    private javax.swing.JTextArea jTxtAreaProducto;
    private javax.swing.JTextArea jTxtAreaResiduo;
    private javax.swing.JTextField jTxtBase;
    private javax.swing.JTextField jTxtNumero;
    // End of variables declaration//GEN-END:variables
}
