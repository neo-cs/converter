/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.cb;

import mx.neocs.cb.ventanas.Splash;
import mx.neocs.cb.ventanas.Ventana;

import java.awt.EventQueue;
import javax.swing.JFrame;

/**
 * Esta clase es la que inicia la aplicación.
 * 
 * @since 2.0.0
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Main implements Runnable {

    /**
     * Este método es el primero que se ejecuta al iniciar la aplicación.
     * @param args los argumentos de la línea de comandos.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Main());
    }

    @Override
    public void run() {
        Splash.loadSplash();
        JFrame.setDefaultLookAndFeelDecorated(true);
        Ventana ventanaPrincipal = new Ventana();
        ventanaPrincipal.setLocationRelativeTo(null);
        ventanaPrincipal.setVisible(true);
        ventanaPrincipal.toFront();
    }
}
