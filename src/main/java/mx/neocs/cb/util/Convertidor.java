/*
 * Convertidor.java 
 * Created on 26/10/08
 *
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.cb.util;

import java.text.MessageFormat;

/**
 * Esta clase contiene lo elementos necesarios para realizar la conversión
 * de un número en base 10 a una base b cualquiera.
 *
 * @author Freddy Barrera (freddy.barrera.moo@hotmail.com)
 * @since 2.3.10
 */
public class Convertidor {

    private static final String MENSAJE_PARA_LOS_COCIENTES = "{0} / {1} = {2}\n";
    private static final String MENSAJE_PARA_LOS_RESIDUOS = "{0} % {1} = {2}\n";
    private static final String MENSAJE_PARA_PARTE_FRACIONARIA = "{0} * {1} = {2}\n";
    private String nuevoEntero;
    private String nuevoFraccionario;
    private String cocientes;
    private String residuos;
    private String enteroProducto;
    private String fraccionProducto;

    /**
     * Crea una nueva instancia del convertidor.
     */
    public Convertidor() {
        nuevoEntero = "";
        nuevoFraccionario = "";
        cocientes = "";
        residuos = "";
        enteroProducto = "";
        fraccionProducto = "";
    }

    /**
     * Esta función realiza la conversión de un número en base 10 a un número
     * en base b. Requiere dos valores una cadena que debe ser el número que
     * se desea convertir y un valor entero que será la base a la que se
     * convertirá.
     * 
     * @param numeroBruto cadena número en base 10
     * @param base se usara para convertir el numeroBruto a esta base
     * 
     * <p>Para tener acceso a los resultados se deben usar los metodos:</p>
     * {@link #getNuevoEntero()}<br>
     * {@link #getNuevoFraccionario()}<br>
     * <p>Donde el primer metodo será para obtener el valor de la parte entera
     * y el segundo será por consiguiente para la parte fraccionaria.</p>
     * @see #getNuevoEntero()
     * @see #getNuevoFraccionario() 
     */
    public void convertir(final int base, String numeroBruto) {
        String[] separado = CadenaUtil.separar(numeroBruto, '.');
        String parteEntera = separado[0];
        String parteFraccionaria = separado[1];

        convertirEntero(base, parteEntera);
        convertirFraccionaria(base, parteFraccionaria);
    }

    private void convertirEntero(final int base, String numeroEntero) {
        if (!numeroEntero.isEmpty()) {
            int cociente = 1;
            int nuevoCociente = Integer.parseInt(numeroEntero);
            StringBuilder nuevoEnteroSb = new StringBuilder();
            StringBuilder cocientesSb = new StringBuilder();
            StringBuilder residuosSb = new StringBuilder();

            while (cociente != 0) {
                cociente = nuevoCociente / base;
                int modulo = nuevoCociente % base;

                String mensaje = MessageFormat
                        .format(MENSAJE_PARA_LOS_COCIENTES, nuevoCociente,
                                base, cociente);
                cocientesSb.append(mensaje);
                mensaje = MessageFormat.format(MENSAJE_PARA_LOS_RESIDUOS,
                        nuevoCociente, base, modulo);
                residuosSb.append(mensaje);
                nuevoCociente = cociente;

                if ((base > 10) && (modulo > 10)) {
                    int valor = modulo - 10 + 'A';
                    char valorComoLetra = (char) valor;

                    nuevoEnteroSb.append(valorComoLetra);
                } else {
                    nuevoEnteroSb.append(modulo);
                }
            }
            nuevoEnteroSb.reverse();
            nuevoEntero = nuevoEnteroSb.toString();
            cocientes = cocientesSb.toString();
            residuos = residuosSb.toString();
        } else {
            nuevoEntero = "0";
        }
    }

    private void convertirFraccionaria(final int base, String numeroFraccionario) {
        StringBuilder nuevoFraccionarioSb;

        if (!numeroFraccionario.isEmpty()) {
            int vueltas = 0; 
            float fraccion = Float.parseFloat(numeroFraccionario);
            float decimal = fraccion;
            nuevoFraccionarioSb = new StringBuilder(".");
            StringBuilder enteroProductoSb = new StringBuilder();
            StringBuilder fraccionProductoSb = new StringBuilder();
            
            do {
                fraccion *= base;
                int parteEntera = (int) fraccion;
                String mensaje = MessageFormat
                        .format(MENSAJE_PARA_PARTE_FRACIONARIA, decimal, base,
                                parteEntera);
                enteroProductoSb.append(mensaje);
                mensaje = MessageFormat.format(MENSAJE_PARA_PARTE_FRACIONARIA, 
                        decimal, base, fraccion);
                fraccionProductoSb.append(mensaje);

                decimal = fraccion = Float.parseFloat(
                        CadenaUtil.separar(String.valueOf(fraccion),
                        '.')[1]);

                if ((base > 10) && (parteEntera > 10)) {
                    int valor = parteEntera - 10 + 'A';
                    char valorComoLetra = (char) valor;
                    nuevoFraccionarioSb.append(valorComoLetra);
                } else {
                    nuevoFraccionarioSb.append(parteEntera);
                }

                vueltas++;
            } while ((decimal != 0.0) && (vueltas < 10));
            
            enteroProducto = enteroProductoSb.toString();
            fraccionProducto = fraccionProductoSb.toString();
        } else {
            nuevoFraccionarioSb = new StringBuilder(".0");
        }
        
        nuevoFraccionario = nuevoFraccionarioSb.toString();
    }

    /**
     * Devuelve el valor del número entero que se convierte de base 10 a base b.
     * @return el nuevoEntero resultado de la conversión.
     */
    public String getNuevoEntero() {
        return nuevoEntero;
    }

    /**
     * Devuelve el valor del número fraccionario que se convierte de base 10
     * a base b.
     * @return el nuevoFraccionario resultado de la conversión.
     */
    public String getNuevoFraccionario() {
        return nuevoFraccionario;
    }

    /**
     * Devuelve una lista con los cocientes.
     * @return los cocientes que se generan durante la conversión.
     */
    public String getCocientes() {
        return cocientes;
    }

    /**
     * Devuelve una lista con los residuos.
     * @return los residuos que se generan durante la conversión.
     */
    public String getResiduos() {
        return residuos;
    }

    /**
     * Devuelve un lista con los productos de la parte entera.
     * @return the enteroProducto los productos de la parte entera.
     */
    public String getEnteroProducto() {
        return enteroProducto;
    }

    /**
     * Devuelve un lista con los productos de la parte fraccionaria.
     * @return the fraccionProducto los producto de la parte fraccionaria.
     */
    public String getFraccionProducto() {
        return fraccionProducto;
    }
}
