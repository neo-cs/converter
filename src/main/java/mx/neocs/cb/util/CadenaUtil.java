/*
 * CadenaUtil.java  
 * Created on 23/10/2008
 *
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.cb.util;

/**
 * Esta clase contiene algunos metodos para trabajar con cadenas.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@hotmail.com)
 * @since 3.5.6
 */
public class CadenaUtil {

    private CadenaUtil() {
    }

    /**
     * Permite invertir el sentido de una cadena ordenando desde el final
     * hacia el principio.
     * 
     * @param cadena la cadena de la cual se invertira el orden.
     * @return una cadena invertida.
     */
    public static String voltear(String cadena) {
        StringBuilder nuevaCadena = new StringBuilder();
        int tamanyoCadena = cadena.length();

        do {
            nuevaCadena.append(cadena.charAt(tamanyoCadena - 1));
            tamanyoCadena--;
        } while (tamanyoCadena > 0);

        return nuevaCadena.toString();
    }

    /**
     * Este método divide una cadena en el punto que se encuentre el
     * carácter indicado.
     * 
     * @param cadena la cadena que será separada.
     * @param buscar el carácter que se empleará para separar la cadena.
     * @return un arreglo con la separación en la cual no se incluye el carácter
     *      a buscar.
     */
    public static String[] separar(String cadena, final char buscar) {
        boolean encontrado = false;
        StringBuilder entero = new StringBuilder();
        StringBuilder decimal = new StringBuilder();
        
        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == buscar) {
                encontrado = true;
            }

            if (!encontrado) {
                entero.append(cadena.charAt(i));
            } else {
                decimal.append(cadena.charAt(i));
            }
        }

        return new String[] {entero.toString(), decimal.toString()};
    }
}
